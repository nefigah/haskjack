--
-- Main.hs
-- Contains program entry point
--
module Main where

import Haskjack

main :: IO ()
main = do {
  putStrLn "\n**  Welcome to HaskJack!  **";
  gameLoop True;
}
