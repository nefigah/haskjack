--
-- Haskjack.hs
-- Contains the logic defining the program flow
--
module Haskjack where

import Numbskell
import Types
import Deck
import Blackjack
import Control.Monad.State
import Data.Char
import System.IO

type ProgramState = StateT (Player, Dealer, Deck) IO Bool

gameLoop :: Bool -> IO ()
gameLoop True = do {
  deck <- newDeck;
  let {
    handMaker = evalInContext (deal 2) (,) (deal 2);
    ((hand1, hand2), deck') = getContextFrom handMaker deck;
    player = Player {
      playerName = "You",
      playerMoney = 100,
      playerBet = 10,
      playerHands = (addToHand emptyHand hand1, emptyHand),
      playerOutcome = Undetermined
    };
    dealer = Dealer { dealerHand = addToHand emptyHand hand2 };
  };
  putStrLn "\n----------------------------";
  continue <- getResultContextFrom moveLoop (player, dealer, deck');
  gameLoop continue;
}
gameLoop False = putStrLn "Thank you for playing!"

moveLoop :: ProgramState
moveLoop = do {
  (player, dealer, _) <- get;
  let {
    (hand1, hand2) = playerHands player;
    handType = analyzeHand (hand1, hand2);
    (moves, outcome) = determineMoves handType;
  };
  doIO (putStrLn ("Dealer: " ++ (show <| getFirstCard) (dealerHand dealer) ++ "[__]"));
  doIO (putStr (cutName player ++ ": " ++ show hand1));
  when (hand2 /= emptyHand) (doIO (putStr ("  (other: " ++ show hand2 ++ ")")));
  doIO (putStr "\n");

  if not (isEmptyList moves)
  then playRound moves
  else finishRound outcome;
} where {
  playRound moves = do {
    move <- doIO (promptForMove moves);
    if move == Quit
    then newContext False
    else do {
      castState (playMove move);
      (player, _, _) <- get;
      if playerOutcome player == Undetermined
      then moveLoop
      else finishRound Nothing;
    }
  }
}

promptForMove :: [Move] -> IO Move
promptForMove moves = do {
  putStrLn "== Type your move below (or \"quit\") ==";
  print `each` moves;
  putStr "> ";
  hFlush stdout;
  choice <- parseMoveChoice moves;
  case choice of {
    Just move -> newContext move;
    Nothing -> putStrLn "(Invalid move)" >> promptForMove moves;
  }
}

parseMoveChoice :: [Move] -> IO (Maybe Move)
parseMoveChoice allowed = do {
  input <- getLine;
  let { parse = tryParseMove <| asTitleCase };
  newContext (parse input);
} where {
    tryParseMove input = do {
      continueIf (input `inList` allMoves);
      let { move = read input };
      continueIf (move == Quit || move `inList` allowed);
      newContext move;
    };
    asTitleCase (initial : rest) = toUpper initial : (morph rest toLower);
    allMoves = morph [Hit ..] show;
}

cutName :: Player -> String
cutName player = take 6 ((playerName player) ++ repeat ' ')

finishRound :: Maybe Outcome -> ProgramState
finishRound handOutcome = do {
  (player, dealer, _) <- get;
  let {
    outcome = case handOutcome of {
      Just result -> result;
      _ -> playerOutcome player;
    }
  };
  case outcome of {
    Standing
    | snd (playerHands player) == emptyHand -> challenge True
    | standing (snd (playerHands player)) -> do {
        challenge False;
        castState switchCurrentHand;
        challenge True;
      }
    | otherwise -> do {
        castState (setOutcome Undetermined);
        castState switchCurrentHand;
        moveLoop;
      };
    Surrendered -> lose (`div` 2);
    Won -> win (* 1);
    GotBlackjack
    | handValue (dealerHand dealer) == 21 -> case (dealerHand dealer) of {
        (Hand { handAces = (_:[]), nonAces = (_:[]) }) -> push;
        _ -> playerBlackjack;
      }
    | otherwise -> playerBlackjack;
    Lost -> lose (* 1);
    Push -> push;
  } 
}

challenge :: Bool -> ProgramState
challenge endGame = do {
  dealerTurn;
  (player, dealer, _) <- get;
  let {
    yourHand = fst (playerHands player);
    yours = handValue yourHand;
    theirHand = dealerHand dealer;
    theirs = handValue theirHand;
    winner = if endGame then win else winHand;
    loser = if endGame then lose else loseHand;
    pusher = if endGame then push else pushHand;
  };
  doIO (putStrLn ("Dealer has: " ++ show theirHand ++ " = " ++ show theirs));
  doIO (putStrLn ("  You have: " ++ show yourHand ++ " = " ++ show yours));
  case compare yours theirs of {
    LT -> (if theirs > 21 then winner else loser) (* 1);
    EQ -> pusher;
    GT -> winner (* 1);
  }
}

dealerTurn :: ProgramState
dealerTurn = do {
  (player, dealer, deck) <- get;
  if handValue (dealerHand dealer) > 16
  then newContext True
  else let {
    (card, deck') = getContextFrom (deal 1) deck;
    newHand = addToHand (dealerHand dealer) card;
    dealer' = dealer { dealerHand = newHand };
  } in do {
    put (player, dealer', deck');
    dealerTurn;
  }
}

playerBlackjack :: ProgramState
playerBlackjack = do {
  doIO (putStrLn "Blackjack!");
  win bigMoney;
} where bigMoney bet = (bet * 3) `div` 2

win :: (Int->Int) -> ProgramState
win multiplier = reportResults True multiplier message
  where message = "Congrats! You won $"

lose :: (Int->Int) -> ProgramState
lose multiplier = reportResults True multiplier message
  where message = "Alas, you lost $"

push :: ProgramState
push = reportResults True (* 0) message
  where message = "Push. You get $"

winHand :: (Int->Int) -> ProgramState
winHand multiplier = reportResults False multiplier message
  where message = "This hand wins $"

loseHand :: (Int->Int) -> ProgramState
loseHand multiplier = reportResults False multiplier message
  where message = "This hand loses $"

pushHand :: ProgramState
pushHand = reportResults False (* 0) message
  where message = "Push. This hand gets $"

reportResults :: Bool -> (Int->Int) -> String -> ProgramState
reportResults isFinal multiplier message = do {
  (player, _, _) <- get;
  let {
    bet = playerBet player;
    result = multiplier bet;
  };
  doIO (putStrLn (message ++ show result));
  when (not isFinal) (doIO (putStr "..."));
  continue <- maybePrompt isFinal;
  newContext continue;
} where {
  maybePrompt True = doIO promptForNextGame;
  maybePrompt False = newContext True;
}

promptForNextGame :: IO Bool
promptForNextGame = do {
  putStrLn "Press \"q\" to quit, or any other key to play again...";
  hFlush stdout;
  buffering <- hGetBuffering stdin;
  hSetBuffering stdin NoBuffering;
  hSetEcho stdin False;
  flushInput;
  input <- getChar;
  hSetEcho stdin True;
  hSetBuffering stdin buffering;
  newContext (toLower input /= 'q');
}
