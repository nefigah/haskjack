--
-- Types.hs
-- Contains Haskjack's data definitions
--
module Types where

import Numbskell
import Control.Monad

data Suit = Hearts | Spades | Clubs | Diamonds deriving (Eq)
instance Show Suit where {
  show Spades   = "♠";
  show Hearts   = "♥";
  show Clubs    = "♣";
  show Diamonds = "♦";
}

data Value = HighAce | Ace | Jack | Queen | King | Value Int deriving (Eq)
instance Show Value where {
  show HighAce = "A";
  show Ace     = "A";
  show Jack    = "J";
  show Queen   = "Q";
  show King    = "K";
  show (Value i) = show i;
}

data Card = Card Value Suit deriving (Eq)
instance Show Card where {
  show (Card value suit) = "[" ++ (show suit) ++ (show value) ++ "]";
}

data Move = Hit | Stand | Double | Split | Surrender | Quit
  deriving (Show, Read, Enum, Bounded, Eq)
data Outcome = Undetermined
             | Standing
             | Push
             | Won
             | GotBlackjack
             | Lost
             | Surrendered
  deriving (Show, Eq)

data HandType = FirstTurn | CanSplit | Blackjack | Bust | Normal
data Player = Player {
  playerName         :: String,
  playerMoney        :: Int,
  playerBet          :: Int,
  playerHands        :: (Hand, Hand),
  playerOutcome      :: Outcome
}
data Dealer = Dealer {
  dealerHand :: Hand
}

data Hand = Hand {
  nonAces   :: [Card],
  handAces  :: [Card],
  wholeHand :: [Card],
  standing  :: Bool
} deriving (Eq)
instance Show Hand where {
  show (Hand { wholeHand = cards }) = join (morph cards show);
}
