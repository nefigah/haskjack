--
-- Blackjack.hs
-- Contains blackjack-specific logic
--
module Blackjack where

import Numbskell
import Types
import Deck
import Control.Monad.State

type GameState = State (Player, Dealer, Deck) ()

setPlayer :: (Player->Player) -> GameState
setPlayer change = changeState playerChanger
  where playerChanger (player, dealer, deck) = (change player, dealer, deck)

setPlayerDeck :: (Player->Deck->(Player,Deck)) -> GameState
setPlayerDeck change = changeState playerAndDeck
  where playerAndDeck (player, dealer, deck) = let {
    (player', deck') = change player deck;
  } in (player', dealer, deck')

addToCurrentHand :: Player -> [Card] -> Player
addToCurrentHand player@(Player { playerHands = (current, other) }) cards = let {
  added = addToHand current cards;
} in player { playerHands = (added, other) }

analyzeHand :: (Hand, Hand) -> HandType
-- Unsplit hand, no aces, exactly 2 cards
analyzeHand (Hand (card1 : card2 : []) [] _ _, Hand [] [] _ _)
  | cardValue card1 == cardValue card2 = CanSplit
  | otherwise                          = FirstTurn
-- Unsplit hand, exactly 1 non-ace card, exactly 1 ace
analyzeHand (Hand (card : []) (_ : []) _ _, Hand [] [] _ _)
  | cardValue card == 10 = Blackjack
  | otherwise            = FirstTurn
-- Unsplit hand, exactly 2 (ace) cards
analyzeHand (Hand [] (_ : _ : []) _ _, Hand [] [] _ _) = CanSplit
-- Otherwise
analyzeHand (hand1, hand2)
  | handValue hand1 > 21 || handValue hand2 > 21 = Bust
  | otherwise                                    = Normal

determineMoves :: HandType -> ([Move], Maybe Outcome)
determineMoves Blackjack = ([], Just GotBlackjack)
determineMoves Bust = ([], Just Lost)
determineMoves CanSplit = ([Stand, Hit, Split, Double, Surrender], Nothing)
determineMoves FirstTurn = ([Stand, Hit, Double, Surrender], Nothing)
determineMoves Normal = ([Stand, Hit], Nothing)

cardValue :: Card -> Int
cardValue (Card (Value val) _) = val
cardValue (Card HighAce _) = 11
cardValue (Card Ace _) = 1
cardValue _ = 10

handValue :: Hand -> Int
handValue (Hand { handAces = aces, nonAces = others }) =
  aggregate tally (aggregate tally 0 others) aces
  where {
    tally card@(Card val _) total =
      let aceVal = case aces of {
        ((Card Ace suit) : []) -> if total < 11 then Card HighAce suit else card;
        _ -> card;
      } in total + cardValue (if val == Ace then aceVal else card);
  }

playMove :: Move -> GameState
playMove Stand = stand
playMove Hit = playerHit
playMove Double = double
playMove Split = split
playMove Surrender = setOutcome Surrendered

stand :: GameState
stand = do {
  standCurrentHand;
  setOutcome Standing;
}

playerHit :: GameState
playerHit = setPlayerDeck dealtCards
  where {
    dealtCards player deck = let {
      (card, deck') = getContextFrom (deal 1) deck;
    } in (addToCurrentHand player card, deck')
  }

standCurrentHand :: GameState
standCurrentHand = setPlayer handStand
  where {
    handStand player = let {
      (hand1, hand2) = playerHands player;
    } in player { playerHands = (hand1 { standing = True }, hand2) };
  }

switchCurrentHand :: GameState
switchCurrentHand = setPlayer selectedHand
  where {
    selectedHand player@(Player { playerHands = (hand1, hand2) }) =
      player { playerHands = (hand2, hand1) };
  }

split :: GameState
split = do {
  setPlayer newHands;
  playerHit;
  switchCurrentHand;
  playerHit;
  switchCurrentHand;
} where {
  newHands player = let {
    (card1 : card2 : _) = wholeHand (fst (playerHands player));
    hand1' = addToHand emptyHand [card1];
    hand2' = addToHand emptyHand [card2];
  } in player { playerHands = (hand1', hand2') };
}

setBet :: (Int->Int) -> GameState
setBet change = setPlayer betAmount
  where {
    betAmount player = player { playerBet = change (playerBet player) };
  }

double :: GameState
double = do {
  setBet (* 2);
  playerHit;
  stand;
}

setOutcome :: Outcome -> GameState
setOutcome to = setPlayer outcome
  where outcome player = player { playerOutcome = to }
