--
-- Deck.hs
-- Contains logic for manipulating decks of playing cards
--
module Deck where

import Numbskell
import Types
import Control.Monad.State
import qualified Data.Vector as Collection

type Deck = (Int, Collection.Vector Card)

newDeck :: IO Deck
newDeck = do {
  rng <- newRNG;
  let { (shuffled, rng') = shuffle rng };
  updateRNG rng';
  newContext (0, shuffled);
}

shuffle :: RNG -> (Collection.Vector Card, RNG)
shuffle rng = getContextFrom shuffled rng
  where {
    shuffled = createFrom 52 randomizeCards;
    randomizeCards position = do {
      generator <- get;
      let { (val, generator') = randomFromRange (position, 51) generator };
      put generator';
      newContext (cards `at` val);
    };
    cards = Collection.fromList [
              Card vals suits
              | vals  <- (Ace : nums) ++ [Jack, Queen, King],
                suits <- [Hearts, Spades, Clubs, Diamonds]
            ];
    nums = [ Value v | v <- [2 .. 10] ];
  }

deal :: Int -> State Deck [Card]
deal amount = doTimes amount dealCard
  where dealCard = do {
      (top, deck) <- get;
      let { card = deck `at` top };
      put (top + 1, deck);
      return card;
  }

emptyHand :: Hand
emptyHand = Hand [] [] [] False

getFirstCard :: Hand -> Card
getFirstCard (Hand { wholeHand = cards }) = head cards

addToHand :: Hand -> [Card] -> Hand
-- Add no cards
addToHand hand [] = hand
-- Add cards, the first of which is an ace
addToHand hand (ace@(Card Ace _) : cards) = addToHand newHand cards
  where {
    newHand = hand {
      handAces = handAces hand ++ [ace],
      wholeHand = wholeHand hand ++ [ace]
    }
  }
-- Add cards, the first of which is a non-ace
addToHand hand (card : cards) = addToHand newHand cards
  where {
    newHand = hand {
      nonAces = nonAces hand ++ [card],
      wholeHand = wholeHand hand ++ [card]
    }
  }
