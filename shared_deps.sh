#!/bin/bash
if [ -r "$1" ] && [ -d "$2" ]; then
    ldd "$1" | \grep '=> /' | awk '{print $3}' | xargs -I '{}' cp -v '{}' "${2/%\//}/"
else
    echo 'Usage: shared_deps.sh exe_file dest_dir'
fi
